package test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class junitTest {

	@Test
	public void createNewUser() {
		Person person = new Person();
		person.setName("daniel");
		person.setAge(36);
		
		Assertions.assertEquals("daniel", person.getName());
	}
}